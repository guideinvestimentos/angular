#!/usr/bin/env sh
export DOLLAR='$'
envsubst < /src/template.conf > /etc/nginx/conf.d/default.conf 
nginx -g 'daemon off;'