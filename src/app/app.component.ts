import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [JsonPipe]
})
export class AppComponent {
  title = 'app';

  constructor(
    private _httpClient: HttpClient
  ) {
    this.isNginx = environment.production == true;
  }

  result: any;
  accessToken: string;
  isNginx: boolean;

  getHealthCheck() {
    this._httpClient
      .get('/healthcheck')
      .subscribe((data) => this.result = data);
  }

  getServiceStatus() {
    this._httpClient
      .get('/service_status')
      .subscribe((data) => this.result = data);
  }

  getTokenSample() {
    this._httpClient
      .post(`${environment.servers.auth}/auth/users/login`, {
        user_name: 'svc-job_data_analysis',
        password: 'Toor@2017'
      })
      .subscribe(
        (data: any) => {
          this.result = data;
          this.accessToken = data.access_token;
        },
        (error) => {
          this.result = error;
        });
  }

  getUserIdentityClasses() {
    this._httpClient
      .get(`${environment.servers.api}/api/org_unit_user_identity/1.0.0/classes`, {
        headers: {
          'Authorization': `Bearer ${this.accessToken}`
        }
      })
      .subscribe(
        (data) => {
          this.result = data;
        },
        (error) => {
          this.result = error;
        });
  }
}
