FROM nginx:alpine
COPY ./nginx/mime.types /etc/nginx/mime.types
COPY ./nginx/template.conf /src/template.conf
COPY ./dist/app /usr/share/nginx/html
COPY ./nginx/docker-entrypoint.sh /src/docker-entrypoint.sh
ENTRYPOINT [ "./src/docker-entrypoint.sh" ]